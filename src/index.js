const { Server } = require("socket.io")
const blockedUsers = require("./blocked.json")
const uuid = require("uuid/v4")
const io = new Server(3007)

const users = {}

io.on("connection", (socket) => {
  const clientIp = socket.request.connection.remoteAddress
  function checkUser() {
    if (!users.clientIp) {
      // did not auth, or is blocked
    }
  }
  socket.on("auth", (e) => {
    if (blockedUsers.clientIp) {
      io.emit("auth-error", { reason: "Ip is blocked" })
      return
    }
    users[clientIp] = {}
    io.emit("auth-complete")
  })
})
